Download
###############################################################################

:slug: download
:modified: 2017-05-19 12:00

.. _Boxes: https://wiki.gnome.org/Apps/Boxes
.. _virt-manager: http://virt-manager.org/
.. _virt-viewer:
.. _virt-manager download page: http://virt-manager.org/download
.. _QEMU: http://www.qemu.org/
.. _spice-gtk-0.36.tar.bz2: /download/gtk/spice-gtk-0.36.tar.bz2
.. _UsbDk_1.0.21_x64.msi: /download/windows/usbdk/UsbDk_1.0.21_x64.msi
.. _UsbDk_1.0.21_x86.msi: /download/windows/usbdk/UsbDk_1.0.21_x86.msi
.. _aSPICE:
.. _aSPICE from play.google.com: https://play.google.com/store/apps/details?id=com.iiordanov.freeaSPICE
.. _Web client: spice-html5.html
.. _OS X client: osx-client.html
.. _launcher-mobile: https://github.com/flexvdi/launcher-mobile
.. _flexVDI: https://flexvdi.com
.. _flexVDI Client at App Store: https://itunes.apple.com/us/app/flexvdi-client/id1051361263?mt=8
.. _flexVDI Client at Play Store: https://play.google.com/store/apps/details?id=com.flexvdi.androidlauncher
.. _spice-vdagent-0.19.0.tar.bz2: /download/releases/spice-vdagent-0.19.0.tar.bz2
.. _xf86-video-qxl:
.. _xf86-video-qxl-0.1.5.tar.bz2: http://xorg.freedesktop.org/releases/individual/driver/xf86-video-qxl-0.1.5.tar.bz2
.. _spice-guest-tools: /download/windows/spice-guest-tools/spice-guest-tools-latest.exe
.. _Windows QXL driver: /download/windows/qxl/qxl-0.1-24/
.. _Windows QXL-WDDM-DOD driver: /download/windows/qxl-wddm-dod/qxl-wddm-dod-0.19/
.. _Changelog: https://gitlab.freedesktop.org/spice/win32/qxl-wddm-dod/raw/master/Changelog
.. _Windows SPICE agent: /download/windows/vdagent/vdagent-win-0.9.0
.. _Spice WebDAV daemon: /download/windows/spice-webdavd/
.. _spice-0.14.1.tar.bz2: /download/releases/spice-server/spice-0.14.1.tar.bz2
.. _spice-0.12.8.tar.bz2: /download/releases/spice-0.12.8.tar.bz2
.. _spice-protocol-0.12.15.tar.bz2: /download/releases/spice-protocol-0.12.15.tar.bz2
.. _usbredir-0.7.1.tar.bz2: /download/usbredir/usbredir-0.7.1.tar.bz2
.. _Xspice: xspice.html
.. _README.xspice: http://cgit.freedesktop.org/xorg/driver/xf86-video-qxl/tree/README.xspice
.. _x11spice: https://gitlab.freedesktop.org/spice/x11spice/repository/archive.tar.bz2?ref=v1.1
.. _folder sharing: spice-user-manual.html#_folder_sharing
.. _Nightly builds: nightly-builds.html
.. _spice-streaming-agent-0.3.tar.xz: /download/releases/spice-streaming-agent/spice-streaming-agent-0.3.tar.xz

Client
++++++
To connect to a virtual machine using SPICE, you need a client application.

GTK+ widget
^^^^^^^^^^^
spice-gtk is a GTK+3 SPICE widget. It features glib-based objects for SPICE protocol parsing and a gtk widget for embedding the SPICE display into other applications such as virt-manager_ or Boxes_. Python and Vala bindings are available too.

- SPICE GTK+ Widget - `spice-gtk-0.36.tar.bz2`_

  - https://gitlab.freedesktop.org/spice/spice-gtk

The recommended client application is virt-viewer_.

Windows installers
^^^^^^^^^^^^^^^^^^
- virt-viewer Windows installer - can be downloaded from `virt-manager download page`_
- UsbDk - A Windows filter driver developed for Spice USB redirection (windows client side) - `UsbDk_1.0.21_x64.msi`_, `UsbDk_1.0.21_x86.msi`_, (`source code </download/windows/usbdk/spice-usbdk-win-1.0-21-sources.zip>`_)

  - https://gitlab.freedesktop.org/spice/win32/usbdk

Other clients
^^^^^^^^^^^^^
- Android client - aSPICE_ is a secure, SSH capable, open source SPICE protocol client that makes use of the LGPL licensed native libspice library. You can find and install `aSPICE from play.google.com`_.
- `Web client`_ - a simple javascript client

  - https://gitlab.freedesktop.org/spice/spice-html5
- Experimental `OS X client`_
- launcher-mobile_ - A GPLv2 licensed cross-platform mobile client for both iOS and Android. Though mainly intended to be used as a client for flexVDI_, it can also connect to conventional SPICE sessions.

  - It is also avaiable in binary form: `flexVDI Client at App Store`_, `flexVDI Client at Play Store`_


Guest
+++++
This section contains various optional drivers and daemons that can be installed on the guest to provide enhanced SPICE integration and performance.

Linux sources
^^^^^^^^^^^^^
- SPICE vdagent - `spice-vdagent-0.19.0.tar.bz2`_

  - https://gitlab.freedesktop.org/spice/linux/vd_agent
- x.org QXL video driver - `xf86-video-qxl-0.1.5.tar.bz2`_; Also contains Xspice

  - http://cgit.freedesktop.org/xorg/driver/xf86-video-qxl


Windows binaries
^^^^^^^^^^^^^^^^
- Windows guest tools - `spice-guest-tools`_

  - https://gitlab.freedesktop.org/spice/spice-nsis

This installer contains some optional drivers and services that can be installed in Windows guest to improve SPICE performance and integration. This includes the qxl video driver and the SPICE guest agent (for copy and paste, automatic resolution switching, ...)

- `Windows QXL-WDDM-DOD driver`_

  - https://gitlab.freedesktop.org/spice/win32/qxl-wddm-dod
  - Changelog_

- `Windows QXL driver`_

  - https://gitlab.freedesktop.org/spice/win32/qxl

`Windows QXL driver`_ is not needed if you are using the Windows guest tools installer above.

- `Windows SPICE agent`_

  - https://gitlab.freedesktop.org/spice/win32/vd_agent

This is not needed if you are using the Windows guest tools installer above.


To enable the `folder sharing`_ in the Windows guest you need:

- `Spice WebDAV daemon`_

  - https://git.gnome.org/browse/phodav/tree/spice

Server
++++++
The SPICE server code is needed when building SPICE support into QEMU_. It should be available as a package in your favourite Linux distribution, which is the preferred way of getting it.

- spice-protocol - headers defining protocols, `spice-protocol-0.12.15.tar.bz2`_

  - https://gitlab.freedesktop.org/spice/spice-protocol

- libusbredir - For USB redirection support, `usbredir-0.7.1.tar.bz2`_

  - https://gitlab.freedesktop.org/spice/usbredir

0.14.1 - stable release
^^^^^^^^^^^^^^^^^^^^^^^
- SPICE - Server - `spice-0.14.1.tar.bz2`_

  - https://gitlab.freedesktop.org/spice/spice

0.12.8 - previous stable series
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
- SPICE - Server - `spice-0.12.8.tar.bz2`_

  - https://gitlab.freedesktop.org/spice/spice/tree/0.12

Xspice server
^^^^^^^^^^^^^
Xspice_ - an X and Spice server. Requires Xorg. See `README.xspice`_.

- The latest sources are `xf86-video-qxl`_. Xspice is mostly reusing that driver and linking directly with spice-server.

x11spice
^^^^^^^^
- x11spice_ - A utility to allow a user to share a current running X session via a Spice server.

  - https://gitlab.freedesktop.org/spice/x11spice

Streaming agent (experimental)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Guest-side daemon which captures the guest video output, encodes it to a video stream and forwards the resulting stream to the host to be sent through SPICE.

- SPICE - Streaming Agent - `spice-streaming-agent-0.3.tar.xz`_

  - https://gitlab.freedesktop.org/spice/spice-streaming-agent


Older releases
++++++++++++++
Older source releases can be found here__. Old versions of spice-gtk can be found `here </download/gtk/>`_.

__ /download/releases/

Nightly Builds
++++++++++++++
`Nightly builds`_ are generated from upstream git master.
